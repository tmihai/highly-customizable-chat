require(["react",
		 "chatinput",
		 "chatmessagelist",
		 "chattab",
		 "chat"
], function(React, ChatInput, ChatMessageList, ChatTab, Chat) {

	var ChatMessage = React.createClass({displayName: "ChatMessage",
		render: function() {
			return (
				React.createElement("div", {className: "chatMessage me"}, 
					React.createElement("span", {className: "chatMessage-author"}, 
						this.props.author
					), 
					React.createElement("span", {className: "chatMessage-text"}, 
						this.props.text
					)
				)
			);
		}
	});

	return ChatMessage;
});