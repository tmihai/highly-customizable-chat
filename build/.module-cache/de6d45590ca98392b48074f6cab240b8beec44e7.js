define([ "react",
		 "chattab"
], function(React, ChatTab) {

	React.render(
		React.createElement(ChatTab, {tabid: "tabid012345", messages: [
		{ id: 'timestamp1', author: "Pete Hunt", text: "This is one message" },
		{ id: 'timestamp2', author: "Jordan Walke", text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur efficitur congue mauris, quis aliquam nisl venenatis sed. Nulla sed pellentesque ipsum, sed sollicitudin eros. Nulla sodales cursus odio, et ultrices quam semper lobortis. Aenean tincidunt, nulla non viverra condimentum, leo massa finibus enim, a tincidunt est magna vel ipsum. Sed sollicitudin est mi, eget imperdiet nisi dignissim eu. Phasellus dictum blandit magna, eget iaculis nunc condimentum in. Integer nunc libero, bibendum non ex non, sollicitudin auctor elit. Suspendisse pulvinar metus eu risus dignissim fringilla. Etiam sodales sollicitudin velit, a fringilla lectus cursus vel. Praesent aliquam eros sagittis velit efficitur congue. Suspendisse et magna faucibus, condimentum tellus a, lacinia neque." }
	]}),
		document.body
	);
	
});