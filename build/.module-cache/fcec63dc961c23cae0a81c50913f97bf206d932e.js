var ChatMessageList = React.createClass({displayName: "ChatMessageList",
	render: function() {
		var messageNodes = this.props.data.map(function (message) {
		    return (
		        React.createElement(ChatMessage, {author: message.author, text: message.text})
		   	);
		});
		return (
		    React.createElement("div", {className: "messageList"}, 
		    	messageNodes
		    )
		);
	}
});