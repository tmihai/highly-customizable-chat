
define([ "react",
		 "components/chattab"
], function(React, ChatTab) {

    React.render(
		React.createElement(ChatTab, null),
		document.body
	);

});