define([ "react",
		 "chattab"
], function(React, ChatTab) {

	React.render(
		React.createElement(ChatTab, {tabid: "tabid012345"}),
		document.body
	);
	
});