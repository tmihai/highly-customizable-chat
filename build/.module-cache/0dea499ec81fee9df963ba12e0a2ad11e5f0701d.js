define([ "ko",
		 "react",
		 "chatmessagelist",
		 "chatinput"
], function(ko, React, ChatMessageList, ChatInput) {

	var ChatTab = React.createClass({displayName: "ChatTab",
		render: function() {
			return (
				React.createElement("div", {className: "chatTab"}, 
					React.createElement(ChatMessageList, {messages: messages}), 
					React.createElement(ChatInput, null)
				)
			);
		}
	});

	return ChatTab;
});