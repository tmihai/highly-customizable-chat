define([ "react",
		 "flux",
		 "constants"
], function(React, 
			flux,
			Constants) {

	var fluxDispatcher = flux.Dispatcher;

	return new fluxDispatcher();
});