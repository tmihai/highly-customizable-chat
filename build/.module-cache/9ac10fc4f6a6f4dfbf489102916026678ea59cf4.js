define([ "react",
		 "components/chatmessage"
], function(React, ChatMessage) {

	var ChatMessageList = React.createClass({displayName: "ChatMessageList",
		render: function() {
			var messageNodes = this.props.messages.map(function (message) {
			    return (
			        React.createElement(ChatMessage, {key: message.timestamp, author: message.author, text: message.text})
			   	);
			});
			return (
			    React.createElement("div", {className: "messageList"}, 
			    	messageNodes
			    )
			);
		}
	});

	return ChatMessageList;
});