define([ "react"
], function(React) {

	var ChatMessage = React.createClass({displayName: "ChatMessage",
		render: function() {
			var className = "chatMessage"
			if (this.props.author !== 'Author') {
				className += " other"
			}

			return (
				React.createElement("div", {className: className}, 
					React.createElement("span", {className: "chatMessage-author"}, 
						this.props.author
					), 
					React.createElement("span", {className: "chatMessage-text"}, 
						this.props.text
					)
				)
			);
		}
	});

	return ChatMessage;
});