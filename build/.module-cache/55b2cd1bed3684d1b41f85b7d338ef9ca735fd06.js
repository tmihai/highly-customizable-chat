define([ "react",
		 "chatmessagelist",
		 "chatinput"
], function(React, ChatMessageList, ChatInput) {

	var ChatTab = React.createClass({displayName: "ChatTab",
		addMessage: function() {
			this.state.messages.push({ timestamp: '2', author: 'Author2', text: 'Message text here...' })
		},
		getInitialState: function(){
			return {
				messages: [
			    	{ timestamp: '2', author: 'Author', text: 'Message text here...' }
			    ]
			}
		},
		render: function() {
			return (
				React.createElement("div", {className: "chatTab angle-gradient"}, 
					React.createElement(ChatMessageList, {messages: this.state.messages}), 
					React.createElement(ChatInput, {sendMessage: this.addMessage})
				)
			);
		}
	});

	return ChatTab;
});