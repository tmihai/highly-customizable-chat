var ChatInput = React.createClass({displayName: "ChatInput",
  render: function() {
    return (
      	React.createElement("div", {className: "chatInput"}, 
	        React.createElement("input", {type: "text"}), 
	        React.createElement("button", null, "Send message")
	    )
	);
  }
});