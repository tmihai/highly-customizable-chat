define([ "react",
		 "chatmessage"
], function(React, ChatMessage) {

	var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

	var ChatMessageList = React.createClass({displayName: "ChatMessageList",
		render: function() {
			var messageNodes = this.props.messages.map(function (message) {
			    return (
			        React.createElement(ChatMessage, {key: message.timestamp, author: message.author, text: message.text})
			   	);
			});
			return (
			    React.createElement("div", {className: "messageList"}, 
			    	React.createElement(ReactCSSTransitionGroup, {transitionName: "example"}, 
			    	messageNodes
			    	)
			    )
			);
		}
	});

	return ChatMessageList;
});