define([ "react",
		 "chatmessagelist",
		 "chatinput",
		 "store"
], function(React, ChatMessageList, ChatInput, Store) {

	var ChatTab = React.createClass({displayName: "ChatTab",
		addMessage: function(inputMessage) {
			this.state.messages.push({ timestamp: new Date().getTime(), author: 'Author2', text: inputMessage });
			this.setState({
				messages: this.state.messages
			});
		},
		getInitialState: function(){
			return {
				messages: [
			    	{ timestamp: new Date().getTime(), author: 'Author', text: 'Message text here...' }
			    ]
			}
		},
		render: function() {
			return (
				React.createElement("div", {className: "chatTab angle-gradient"}, 
					React.createElement(ChatMessageList, {messages: this.state.messages}), 
					React.createElement(ChatInput, {sendMessage: this.addMessage})
				)
			);
		}
	});

	return ChatTab;
});