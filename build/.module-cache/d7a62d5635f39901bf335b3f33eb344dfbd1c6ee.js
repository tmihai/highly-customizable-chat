define([ "react",
		 "chatmessagelist",
		 "chatinput"
], function(React, ChatMessageList, ChatInput, Store, Actions) {
	
	function MessagesStore() {
		var self = this;
		
		self.messages = [];
		
		self.init = function() {
			self.messages = [
								{ 
									timestamp: new Date().getTime(), 
									author: 'Author', 
									text: 'Message text here...' 
								}
							];
		}
	}
	
	var ChatTab = React.createClass({displayName: "ChatTab",
		getInitialState: function(){
			return {
				messages: MessagesStore.getMessages()
			};
		},
		sendMessage: function(messageText) {
			this.state.messages.push(
				{ 
					timestamp: new Date().getTime(), 
					author: 'Author2', 
					text: messageText 
				}
			);
			this.setState({
				messages: this.state.messages
			});
		},
		render: function() {
			return (
				React.createElement("div", {className: "chatTab angle-gradient"}, 
					React.createElement(ChatMessageList, {messages: this.state.messages}), 
					React.createElement(ChatInput, {sendMessage: this.sendMessage})
				)
			);
		}
	});

	return ChatTab;
});