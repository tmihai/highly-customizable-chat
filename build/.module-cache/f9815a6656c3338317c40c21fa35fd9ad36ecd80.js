define([ "react",
         "dispatcher",
         "constants"
], function(React, Dispatcher, Constants) {

  var EventEmitter = require('events').EventEmitter;
  var merge = require('react/lib/merge');

  var CHANGE_EVENT = 'change';

  var _appState = {
    data: []
  };

  function generateRandom() {
    return (Math.random() * Date.now() | 0).toString(36);
  }

  function sendMessage(content) {
    _appState.data.push({ 
      timestamp: new Date().getTime(), 
      author: 'Author2', 
      text: inputMessage
    });
  }

  var Store = merge(EventEmitter.prototype, {
    emitChange: function () {
      this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
      this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
      this.removeListener(CHANGE_EVENT, callback);
    },

    getAppState: function () {
      return _appState;
    },

    dispatcherIndex: Dispatcher.register(function (payload) {
      var action = payload.action;

      switch (action.actionType) {
        case Constants.SEND_MESSAGE: {
          sendMessage(action.content);
          Store.emitChange();
          break;
        }
        // likewise, create more cases to handle other constants & actions
      }

      return true; // return promise
    })
  });

  module.exports = Store;
});