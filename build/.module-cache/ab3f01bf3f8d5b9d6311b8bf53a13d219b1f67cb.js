define([ "react"
], function(React) {

	var ChatMessage = React.createClass({displayName: "ChatMessage",
		render: function() {
			return (
				React.createElement("div", {className: "chatActionbar"}, 
					React.createElement("div", {className: "name"}, 
						"Someone"
					), 
					React.createElement("div", {className: "actions"}, 
						React.createElement("div", {className: "icon icon-gear opacity-enter"}), 
						React.createElement("div", {className: "icon icon-close opacity-enter"})
					)
				)
			);
		}
	});

	return ChatMessage;
});