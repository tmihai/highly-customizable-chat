var data = [
  {author: "Pete Hunt", text: "This is one message"},
  {author: "Jordan Walke", text: "This is *another* message"}
];
var ChatTab = React.createClass({displayName: "ChatTab",
  render: function() {
    return (
    	React.createElement("div", {className: "chaatTab"}, 
	   		React.createElement(ChatMessageList, {data: data}), 
	   		React.createElement(ChatInput, null)
	   	)
	);
  }
});