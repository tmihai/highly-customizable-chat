define([ "react",
		 "chatmessagelist",
		 "chatinput",
		 "store"
], function(React, ChatMessageList, ChatInput, Store) {

	var ChatTab = React.createClass({displayName: "ChatTab",
		render: function() {
			return (
				React.createElement("div", {className: "chatTab angle-gradient"}, 
					React.createElement(ChatMessageList, {messages: Store.getAppState().data}), 
					React.createElement(ChatInput, {sendMessage: this.addMessage})
				)
			);
		}
	});

	return ChatTab;
});