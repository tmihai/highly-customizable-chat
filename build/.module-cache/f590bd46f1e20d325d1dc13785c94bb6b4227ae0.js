require(["../build/react",
		 "../build/components/chatinput",
		 "../build/components/chatmessagelist",
		 "../build/components/chattab",
		 "../build/chat"
], function(React, ChatInput, ChatMessageList, ChatTab, Chat) {

	var ChatInput = React.createClass({displayName: "ChatInput",
		render: function() {
			return (
				React.createElement("div", {className: "chatInput"}, 
					React.createElement("input", {type: "text", className: "chatInput-input", placeholder: "Chat message..."}), 
					React.createElement("input", {type: "button", className: "chatInput-button", value: "Send"})
				)
			);
		}
	});
	
	return ChatInput;
});