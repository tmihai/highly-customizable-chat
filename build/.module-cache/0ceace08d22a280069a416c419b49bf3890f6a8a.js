define([ "react"
], function(React) {

	var ChatInput = React.createClass({displayName: "ChatInput",
		getInitialState: function() {
			return { value: 'Hello!' };
		},
		handleChange: function(event) {
			this.setState({value: event.target.value});
		},
		render: function() {
			return (
				React.createElement("div", {className: "chatInput"}, 
					React.createElement("input", {type: "text", className: "chatInput-input", placeholder: "Chat message...", value: this.state.inputmessage, onChange: this.inputMessageChange}), 
					React.createElement("input", {type: "button", className: "chatInput-button", value: "Send", onClick: this.props.sendMessage})
				)
			);
		}
	});

	return ChatInput;
});