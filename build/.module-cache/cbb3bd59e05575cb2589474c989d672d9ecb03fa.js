define([ "react",
		 "flux",
		 "react/lib/copyProperties",
		 "constants"
], function(React, 
			flux, copyProperties, 
			Constants) {

	var fluxDispatcher = flux.Dispatcher;

	module.exports = new fluxDispatcher();
});