define([ "react",
		 "chattab"
], function(React, ChatTab) {

	var jsonMessages = [{timestamp: '1', author: 'Author', text: 'Message text here...'}];

	React.render(
		React.createElement(ChatTab, {messages: jsonMessages}),
		document.body
	);
	
});