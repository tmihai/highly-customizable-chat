require(["../build/react",
		 "../build/components/chatinput",
		 "../build/components/chatmessagelist",
		 "../build/components/chattab",
		 "../build/chat"
], function(React, ChatInput, ChatMessageList, ChatTab, Chat) {

	React.render(
		React.createElement(ChatTab, {tabid: "tabid012345"}),
		document.body
	);
	
});