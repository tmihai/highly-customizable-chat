define([ "react",
		 "dispatcher",
		 "constants"
], function(React, Dispatcher, Constants) {

	return {
	  sendMessage: function (content) {
	    Dispatcher.handleViewAction({
	      actionType: Constants.SEND_MESSAGE,
	      content: content
	    });
	  }
	  // add more methods likewise
	};
});