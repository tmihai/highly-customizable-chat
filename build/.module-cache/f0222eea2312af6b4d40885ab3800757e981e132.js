define([ "react",
		 "chatmessagelist",
		 "chatinput"
], function(React, ChatMessageList, ChatInput, Store, Actions) {
	
	var _appState = {
    	data: [{ timestamp: new Date().getTime(), author: 'Author', text: 'Message text here...' }]
  	};
	
	var ChatTab = React.createClass({displayName: "ChatTab",
		getAppState: function () {
			return _appState;
    	},
		sendMessage: function(messageText) {
			_appState.data.push(
				{ timestamp: new Date().getTime(), author: 'Author', text: messageText }
			)
		},
		render: function() {
			return (
				React.createElement("div", {className: "chatTab angle-gradient"}, 
					React.createElement(ChatMessageList, {messages: _appState.data}), 
					React.createElement(ChatInput, {sendMessage: this.sendMessage})
				)
			);
		}
	});

	return ChatTab;
});