// For any third party dependencies, like jQuery, place them in the lib folder.

// Configure loading modules from the lib directory,
// except for 'app' ones, which are in a sibling
// directory.
requirejs.config({
    paths: {
    	"react": 		   "build/react",
        "chat": 		   "build/components/chat",
        "chattab": 		   "build/components/chattab",
        "chatmessagelist": "build/components/chatmessagelist",
        "chatmessage": 	   "build/components/chatmessage",
        "chatinput": 	   "build/components/chatinput"
    }
});

// Start loading the main app file. Put all of
// your application logic in there.
requirejs(['build/main']);