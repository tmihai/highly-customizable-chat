define([ "react"
], function(React) {

	var ChatInput = React.createClass({displayName: "ChatInput",
		getInitialState: function() {
			return { inputMessage: '' };
		},
		inputMessageChange: function(event) {
			this.setState({
				inputMessage: event.target.value
			});
		},
		sendMessage: function() {
			if (this.state.inputMessage) {
				this.props.sendMessage(this.state.inputMessage);
				this.setState({
					inputMessage: ''
				});
			}
		},
		onKeyUp: function(event) {
			console.log(JSON.stringify(event));
			if (event.key === 13) {
				this.sendMessage();
			}
		},
		render: function() {
			return (
				React.createElement("div", {className: "chatInput"}, 
					React.createElement("input", {	type: "text", 
							className: "chatInput-input", 
							placeholder: "Chat message...", 
							value: this.state.inputMessage, 
							onChange: this.inputMessageChange, 
							onKeyUp: this.onKeyUp}), 
					React.createElement("input", {	type: "button", 
							className: "chatInput-button", 
							value: "Send"})
				)
			);
		}
	});

	return ChatInput;
});