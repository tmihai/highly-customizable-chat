define([ "react",
		 "chattab"
], function(React, ChatTab) {

	var Chat = React.createClass({displayName: "Chat",
		incrementCount: function(){
    		this.setState({
    			count: this.state.count + 1
    		});
		},
		getInitialState: function(){
    		return {
    			count: 0
    		}
		},
		render: function() {
			var jsonMessages = [
		    	{ timestamp: '1', author: 'Author', text: 'Message text here...' }
		    ];
			return (
				React.createElement(ChatTab, {messages: jsonMessages})
			);
		}
	});

	return Chat;
});

