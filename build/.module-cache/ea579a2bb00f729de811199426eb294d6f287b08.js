var data = [
  {author: "Pete Hunt", text: "This is one comment"},
  {author: "Jordan Walke", text: "This is *another* comment"}
];
var ChatTab = React.createClass({displayName: "ChatTab",
  render: function() {
    return (
    	React.createElement("div", null, "Hello ", this.props.tabid, 
	   		React.createElement(ChatMessage, {messageid: "message1"}), 
	   		React.createElement(ChatMessage, {messageid: "message2"}), 
	   		React.createElement(ChatMessage, {messageid: "message3"}), 
	   		React.createElement(ChatMessage, {messageid: "message4"}), 
	   		React.createElement(ChatMessage, {messageid: "message5"})
	   	)
	);
  }
});