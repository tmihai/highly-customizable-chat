// For any third party dependencies, like jQuery, place them in the lib folder.

// Configure loading modules from the lib directory,
// except for 'app' ones, which are in a sibling
// directory.
/*
requirejs.config({
    paths: {
    	"react": 		   "react",
    	"flux": 		   "flux",
    	"constants":       "constants/constants",
    	"dispatcher":      "dispatchers/dispatcher",
    	"actions":         "actions/actions",
    	"store":           "stores/store",
        "chat": 		   "components/chat",
        "chattab": 		   "components/chattab",
        "chatmessagelist": "components/chatmessagelist",
        "chatmessage": 	   "components/chatmessage",
        "chatinput": 	   "components/chatinput"
    }
});
*/

console.log(JSON.stringify(requirejs.config.paths));

// Start loading the main app file. Put all of
// your application logic in there.
requirejs(['main']);