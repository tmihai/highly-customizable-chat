define([ "react"
], function(React) {

	var ChatInput = React.createClass({displayName: "ChatInput",
		render: function() {
			return (
				React.createElement("div", {className: "chatInput"}, 
					React.createElement("input", {type: "text", className: "chatInput-input", placeholder: "Chat message..."}), 
					React.createElement("input", {type: "button", className: "chatInput-button", value: "Send", onClick: this.props.sendMessage})
				)
			);
		}
	});

	return ChatInput;
});