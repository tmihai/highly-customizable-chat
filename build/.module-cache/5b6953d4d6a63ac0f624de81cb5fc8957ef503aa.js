define([ "react",
		 "chatmessage"
], function(React, ChatMessage) {

	var ChatMessageList = React.createClass({displayName: "ChatMessageList",
		render: function() {
			var messageNodes = ko.computed(function() {
				this.props.messages().map(function (message) {
				    return (
				        React.createElement(ChatMessage, {author: message.author, text: message.text})
				   	);
				});
			});
			return (
			    React.createElement("div", {className: "messageList"}, 
			    	messageNodes
			    )
			);
		}
	});

	return ChatMessageList;
});