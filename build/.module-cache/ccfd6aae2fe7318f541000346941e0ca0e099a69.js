define([ "ko",
		 "react",
		 "chatmessagelist",
		 "chatinput"
], function(ko, React, ChatMessageList, ChatInput) {

	var ChatTab = React.createClass({displayName: "ChatTab",
		render: function() {
			return (
				React.createElement("div", {className: "chatTab"}, 
					React.createElement(ChatMessageList, {data: this.props.messages}), 
					React.createElement(ChatInput, null)
				)
			);
		}
	});

	return ChatTab;
});