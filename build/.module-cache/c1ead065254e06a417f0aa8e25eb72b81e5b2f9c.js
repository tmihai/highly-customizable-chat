var ChatInput = React.createClass({displayName: "ChatInput",
  render: function() {
    return (
      	React.createElement("div", {className: "chatInputWrapper"}, 
	        React.createElement("input", {type: "text", className: "chatInput"}), 
	        React.createElement("button", null, "Send message")
	    )
	);
  }
});