var ChatMessage = React.createClass({displayName: "ChatMessage",
  render: function() {
    return (
      	React.createElement("div", {className: "chatMessage"}, 
	        React.createElement("h2", {className: "chatMessage-author"}, 
	        	this.props.author
	        ), 
	        React.createElement("h4", null, 
	        	this.props.children.toString()
	        )
	    )
	);
  }
});