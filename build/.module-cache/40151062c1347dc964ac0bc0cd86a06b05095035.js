var data = [
  {author: "Pete Hunt", text: "This is one message"},
  {author: "Jordan Walke", text: "This is *another* message"}
];
var ChatMessageList = React.createClass({displayName: "ChatMessageList",
	render: function() {
		var messageNodes = this.props.data.map(function (message) {
		    return (
		        React.createElement(ChatMessage, {author: message.author}, 
		        	message.text
		        )
		   	);
		});
		return (
		    React.createElement("div", {className: "messageList"}, 
		    	messageNodes
		    )
		);
	}
})
var ChatTab = React.createClass({displayName: "ChatTab",
  render: function() {
    return (
    	React.createElement("div", null, "Hello ", this.props.tabid, 
	   		React.createElement(ChatMessageList, {data: data})
	   	)
	);
  }
});