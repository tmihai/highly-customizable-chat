define([ "react",
		 "keyMirror"
], function(React, keyMirror) {

	module.exports = keyMirror({
	  SEND_MESSAGE: null
	  // add more constants for more actions
	});
});