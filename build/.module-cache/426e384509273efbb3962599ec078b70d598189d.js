define([ "react",
		 "chatinput",
		 "chatmessagelist",
		 "chattab"
], function(React, ChatInput, ChatMessageList, ChatTab) {

	React.render(
		React.createElement(ChatTab, {tabid: "tabid012345"}),
		document.body
	);
	
});