// For any third party dependencies, like jQuery, place them in the lib folder.

// Configure loading modules from the lib directory,
// except for 'app' ones, which are in a sibling
// directory.
requirejs.config({
    baseUrl: 'components',
    paths: {
    	"react": 		   "build/react",
        "chat": 		   "components/chat",
        "chattab": 		   baseUrl + '/chattab',
        "chatmessagelist": baseUrl + '/chatmessagelist',
        "chatmessage": 	   baseUrl + '/chatmessage',
        "chatinput": 	   baseUrl + '/chatinput'
    }
});

// Start loading the main app file. Put all of
// your application logic in there.
requirejs(['main']);