define([ "react",
		 "dispatcher",
		 "constants"
], function(React, Dispatcher, Constants) {

	module.exports = {
	  createRow: function (content) {
	    Dispatcher.handleViewAction({
	      actionType: Constants.CREATE_ROW,
	      content: content
	    });
	  }
	  // add more methods likewise
	};
});