define([ "react",
		 "dispatchers/dispatcher",
		 "constants/constants"
], function(React, Dispatcher, Constants) {

	return {
	  sendMessage: function (content) {
	    Dispatcher.handleViewAction({
	      actionType: Constants.SEND_MESSAGE,
	      content: content
	    });
	  }
	  // add more methods likewise
	};
});