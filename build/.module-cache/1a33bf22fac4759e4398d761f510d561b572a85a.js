define([ "react",
		 "flux",
		 "constants"
], function(React, 
			flux,
			Constants) {

	var fluxDispatcher = flux.Dispatcher;

	module.exports = new fluxDispatcher();
});