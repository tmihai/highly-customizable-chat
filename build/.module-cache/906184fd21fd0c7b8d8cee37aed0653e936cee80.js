define([ "react",
		 "chatmessagelist",
		 "chatinput"
], function(React, ChatMessageList, ChatInput, Store, Actions) {
	
	function MessagesStore() {
		var self = this;
		
		self.messages = [];
		
		self.init = function() {
			self.messages = [
								{ 
									timestamp: new Date().getTime(), 
									author: 'Author', 
									text: 'Message text here...' 
								}
							];
		};
		
		self.addMessage = function(messageText) {
			self.messages.push(
				{ 
					timestamp: new Date().getTime(), 
					author: 'Author2', 
					text: messageText 
				}
			);
			return self.messages;
		}
	}
	
	var messagesStore = new MessagesStore();
	
	var ChatTab = React.createClass({displayName: "ChatTab",
		getInitialState: function(){
			return {
				messages: messagesStore.messages
			};
		},
		sendMessage: function(messageText) {
			this.setState({
				messages: messagesStore.addMessage(messageText)
			});
		},
		render: function() {
			return (
				React.createElement("div", {className: "chatTab angle-gradient"}, 
					React.createElement(ChatMessageList, {messages: this.state.messages}), 
					React.createElement(ChatInput, {sendMessage: this.sendMessage})
				)
			);
		}
	});

	return ChatTab;
});