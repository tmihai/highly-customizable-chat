define([ "react"
], function(React) {

	var ChatInput = React.createClass({displayName: "ChatInput",
		getInitialState: function() {
			return { inputMessage: '' };
		},
		inputMessageChange: function(event) {
			if (event.target.value) {
				this.setState({
					inputMessage: event.target.value
				});
			}
		},
		sendMessage: function() {
			this.props.sendMessage(this.state.inputMessage);
			this.setState({
				inputMessage: ''
			});
		},
		render: function() {
			return (
				React.createElement("div", {className: "chatInput"}, 
					React.createElement("input", {	type: "text", 
							className: "chatInput-input", 
							placeholder: "Chat message...", 
							value: this.state.inputMessage, 
							onChange: this.inputMessageChange}), 
					React.createElement("input", {	type: "button", 
							className: "chatInput-button", 
							value: "Send", 
							onClick: this.sendMessage})
				)
			);
		}
	});

	return ChatInput;
});