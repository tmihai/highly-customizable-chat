define([ "react",
		 "react/lib/keyMirror"
], function(React, keyMirror) {

	module.exports = keyMirror({
	  CREATE_ROW: null
	  // add more constants for more actions
	});
});
