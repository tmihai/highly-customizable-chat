define([ "react",
		 "components/chatmessagelist",
		 "components/chatinput",
		 "stores/store",
		 "actions/actions"
], function(React, ChatMessageList, ChatInput, Store, Actions) {

	var ChatTab = React.createClass({displayName: "ChatTab",
		sendMessage: function(messageText) {
    		Actions.sendMessage(messageText);
		},
		render: function() {
			return (
				React.createElement("div", {className: "chatTab angle-gradient"}, 
					React.createElement(ChatMessageList, {messages: Store.getAppState().data}), 
					React.createElement(ChatInput, {sendMessage: this.sendMessage})
				)
			);
		}
	});

	return ChatTab;
});