define([ "react"
], function(React) {

	var ChatMessage = React.createClass({displayName: "ChatMessage",
		render: function() {
			return (
				React.createElement("div", {className: "chatActionbar"}, 
					React.createElement("span", {className: "chatName"}, 
						"Someone"
					), 
					React.createElement("span", {className: "toolbar"}, 
						React.createElement("span", {className: "icons settings"}), 
						React.createElement("span", {className: "icons close"})
					)
				)
			);
		}
	});

	return ChatMessage;
});