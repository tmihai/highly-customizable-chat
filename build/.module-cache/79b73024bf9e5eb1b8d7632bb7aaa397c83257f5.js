define([ "react",
		 "flux",
		 "constants"
], function(React, flux, Constants) {

	return new flux.Dispatcher();
});