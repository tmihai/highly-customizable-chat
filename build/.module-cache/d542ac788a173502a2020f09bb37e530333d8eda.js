define([ "react",
		 "chatmessagelist",
		 "chatinput"
], function(React, ChatMessageList, ChatInput) {

	var ChatTab = React.createClass({displayName: "ChatTab",
		render: function() {
			return (
				React.createElement("div", {className: "chatTab"}, 
					React.createElement(ChatMessageList, {messages: this.props.messages}), 
					React.createElement(ChatInput, null)
				)
			);
		}
	});

	return ChatTab;
});