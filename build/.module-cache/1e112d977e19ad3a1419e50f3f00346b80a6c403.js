
define([ "react",
		 "chattab"
], function(React, ChatTab) {
    //This function is called when scripts/helper/util.js is loaded.
    //If util.js calls define(), then this function is not fired until
    //util's dependencies have loaded, and the util argument will hold
    //the module value for "helper/util".
    React.render(
		React.createElement(ChatTab, {messages: [{timestamp: '1', author: 'Author', text: 'Message text here...'}]}),
		document.body
	);
});