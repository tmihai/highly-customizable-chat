define([ "react",
		 "chattab"
], function(React, ChatTab) {

	var jsonMessages = [{timestamp: '1', author: 'Author', text: 'Message text here...'}];

	React.render(
		React.createElement(ChatTab, {tabid: "tabid012345", data: jsonMessages}),
		document.body
	);
	
});