define([ "react"
], function(React, ChatMessageList, ChatTab, Chat) {

	var ChatInput = React.createClass({displayName: "ChatInput",
		render: function() {
			return (
				React.createElement("div", {className: "chatInput"}, 
					React.createElement("input", {type: "text", className: "chatInput-input", placeholder: "Chat message..."}), 
					React.createElement("input", {type: "button", className: "chatInput-button", value: "Send"})
				)
			);
		}
	});

	return ChatInput;
});