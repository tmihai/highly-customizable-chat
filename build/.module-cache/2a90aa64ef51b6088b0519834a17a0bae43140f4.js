var ChatMessage = React.createClass({displayName: "ChatMessage",
  render: function() {
    return (
      	React.createElement("div", {className: "message"}, 
	        React.createElement("h2", {className: "messageAuthor"}, 
	        	this.props.author
	        ), 
	        React.createElement("span", {dangerouslySetInnerHTML: {__html: this.props.children.toString()}})
	    )
	);
  }
});