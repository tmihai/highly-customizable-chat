define([ "react",
		 "chatmessagelist",
		 "chatinput"
], function(React, ChatMessageList, ChatInput) {

	var ChatTab = React.createClass({displayName: "ChatTab",
		incrementCount: function(){
			this.setState({
				count: this.state.count + 1
			});
		},
		getInitialState: function(){
			return {
				count: 0,
				messages: [
			    	{ timestamp: '2', author: 'Author', text: 'Message text here...' }
			    ]
			}
		},
		render: function() {
			return (
				React.createElement("div", {className: "chatTab angle-gradient"}, 
					React.createElement(ChatMessageList, {messages: this.state.messages}), 
					React.createElement(ChatInput, null)
				)
			);
		}
	});

	return ChatTab;
});