
define([ "react",
		 "chat"
], function(React, Chat) {

    React.render(
		React.createElement(Chat, null),
		document.body
	);
	
});