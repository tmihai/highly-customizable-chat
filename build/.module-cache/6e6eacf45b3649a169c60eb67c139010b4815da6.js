define([ "react",
		 "chattab"
], function(React, ChatTab) {

	React.render(
		React.createElement(ChatTab, {messages: [{timestamp: '1', author: 'Author', text: 'Message text here...'}]}),
		document.body
	);
	
})