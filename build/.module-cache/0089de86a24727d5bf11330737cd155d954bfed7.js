define([ "react",
		 "chatmessagelist",
		 "chatinput",
		 "MessagesStore",
		 "MessagesDispatcher"
], function(React, ChatMessageList, ChatInput, MessagesStore, MessagesDispatcher) {
	
	var ChatTab = React.createClass({displayName: "ChatTab",
		getInitialState: function(){
			return {
				messages: MessagesStore.messages
			};
		},
		sendMessage: function(messageText) {
			MessagesStore.addMessage(messageText)
			this.setState({
				messages: MessagesStore.messages
			});
		},
		render: function() {
			return (
				React.createElement("div", {className: "chatTab"}, 
					React.createElement(ChatActionBar, null), 
					React.createElement(ChatMessageList, {messages: this.state.messages}), 
					React.createElement(ChatInput, {sendMessage: this.sendMessage})
				)
			);
		}
	});

	return ChatTab;
});