var ChatMessage = React.createClass({displayName: "ChatMessage",
  render: function() {
    return (
      	React.createElement("div", {className: "chatMessage"}, 
	        React.createElement("span", {className: "chatMessage-author"}, 
	        	this.props.author
	        ), 
	        React.createElement("span", {className: "chatMessage-text"}, 
	        	this.props.children.toString()
	        )
	    )
	);
  }
});