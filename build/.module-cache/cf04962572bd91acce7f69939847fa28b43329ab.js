define([ "react"
], function(React) {

	var ChatMessage = React.createClass({displayName: "ChatMessage",
		render: function() {
			return (
				React.createElement("div", {className: "chatActionbar"}, 
					React.createElement("div", {className: "name"}, 
						"Someone"
					), 
					React.createElement("div", {className: "actions"}, 
						React.createElement("div", {className: "icons settings"}), 
						React.createElement("div", {className: "icons close"})
					)
				)
			);
		}
	});

	return ChatMessage;
});