define([ "react",
		 "flux",
		 "constants/constants"
], function(React, flux, Constants) {

	return new flux.Dispatcher();
});