define([ "react",
		 "flux",
		 "react/lib/copyProperties",
		 "constants"
], function(React, 
			flux, copyProperties, 
			Constants) {

	var fluxDispatcher = flux.Dispatcher;

	module.exports = copyProperties(new fluxDispatcher, {
	  handleViewAction: function (action) {
	    this.dispatch({
	      source: 'VIEW_ACTION',
	      action: action
	    });
	  }
	  // add more methods for other action sources like the server
	});
});