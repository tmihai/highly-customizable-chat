define([ "react"
], function(React) {

	var ChatInput = React.createClass({displayName: "ChatInput",
		getInitialState: function() {
			return { inputMessage: 'Hello!' };
		},
		inputMessageChange: function(event) {
			this.setState({
				inputMessage: event.target.value
			});
		},
		render: function() {
			return (
				React.createElement("div", {className: "chatInput"}, 
					React.createElement("input", {type: "text", className: "chatInput-input", placeholder: "Chat message...", value: this.state.inputMessage, onChange: this.inputMessageChange}), 
					React.createElement("input", {type: "button", className: "chatInput-button", value: "Send", onClick: this.props.sendMessage})
				)
			);
		}
	});

	return ChatInput;
});