var ChatInput = React.createClass({displayName: "ChatInput",
  render: function() {
    return (
      	React.createElement("div", {className: "chatInput"}, 
	        React.createElement("input", {type: "text", className: "chatInput-input"}), 
	        React.createElement("input", {type: "button", className: "chatInput-button", value: "Send"})
	    )
	);
  }
});