define([ "react"
], function(React) {

	var ChatInput = React.createClass({displayName: "ChatInput",
		getInitialState: function() {
			return { inputMessage: '' };
		},
		inputMessageChange: function(event) {
			this.setState({
				inputMessage: event.target.value
			});
		},
		sendMessage: function() {
			if (this.state.inputMessage) {
				this.props.sendMessage(this.state.inputMessage);
				this.setState({
					inputMessage: ''
				});
			}
		},
		onKeyUp: function(event) {
			if (event.key === "Enter") {
				this.sendMessage();
			}
		},
		render: function() {
			return (
				React.createElement("div", {className: "chatinput"}, 
					React.createElement("input", {	type: "text", 
							className: "chatinput-input", 
							placeholder: "Send message...", 
							value: this.state.inputMessage, 
							onChange: this.inputMessageChange, 
							onKeyUp: this.onKeyUp})
				)
			);
		}
	});

	return ChatInput;
});