define([ "react"
], function(React) {

	var ChatMessage = React.createClass({displayName: "ChatMessage",
		render: function() {
			var className;
			if (this.props.author === 'Author2') {
				className = "chatmsg me";
			} else {
				className = "chatmsg other";
			}

			return (
				React.createElement("div", {className: className}, 
					React.createElement("span", {className: "chatmsg-author"}, 
						this.props.author
					), 
					React.createElement("span", {className: "chatmsg-text"}, 
						this.props.text
					)
				)
			);
		}
	});

	return ChatMessage;
});