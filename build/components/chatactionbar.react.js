define([ "react"
], function(React) {

	var ChatMessage = React.createClass({displayName: "ChatMessage",
		render: function() {
			return (
				React.createElement("div", {className: "actionbar"}, 
					React.createElement("div", {className: "name"}, 
						"Someone"
					), 
					React.createElement("div", {className: "actions"}, 
						React.createElement("div", {className: "action opacity-enter"}, 
							React.createElement("span", {className: "icon icon-settings"})
						), 
						React.createElement("div", {className: "action opacity-enter"}, 
							React.createElement("span", {className: "icon icon-close"})
						)
					)
				)
			);
		}
	});

	return ChatMessage;
});