
define([ "react",
		 "chattab"
], function(React, ChatTab) {

    React.render(
		<ChatTab />,
		document.getElementById('app')
	);

});