define([ "react",
		 "chattab"
], function(React, ChatTab) {
	
	function MessagesStore() {
		var self = this;
		
		self.messages = [];
		
		self.initialize = function() {
			self.messages = [
								{ 
									timestamp: new Date().getTime(), 
									author: 'Author', 
									text: 'Message text here...' 
								}
							];
		};
		
		self.addMessage = function(messageText) {
			self.messages.push(
				{ 
					timestamp: new Date().getTime(), 
					author: 'Author2', 
					text: messageText 
				}
			);
			return self.messages;
		};
		
		self.initialize();
	}
	
	return new MessagesStore();
});