// For any third party dependencies, like jQuery, place them in the lib folder.

// Configure loading modules from the lib directory,
// except for 'app' ones, which are in a sibling
// directory.
requirejs.config({
    paths: {
    	"react": 		      "react",
		"MessagesDispatcher": "dispatchers/MessagesDispatcher",
		"MessagesStore":      "stores/MessagesStore",
        "chat": 		      "components/chat.react",
        "chattab": 		      "components/chattab.react",
        "chatactionbar":      "components/chatactionbar.react",
        "chatmessagelist":    "components/chatmessagelist.react",
        "chatmessage": 	      "components/chatmessage.react",
        "chatinput": 	      "components/chatinput.react"
    }
});

// Start loading the main app file. Put all of
// your application logic in there.
requirejs(['main']);