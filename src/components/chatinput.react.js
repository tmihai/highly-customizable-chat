define([ "react"
], function(React) {

	var ChatInput = React.createClass({
		getInitialState: function() {
			return { inputMessage: '' };
		},
		inputMessageChange: function(event) {
			this.setState({
				inputMessage: event.target.value
			});
		},
		sendMessage: function() {
			if (this.state.inputMessage) {
				this.props.sendMessage(this.state.inputMessage);
				this.setState({
					inputMessage: ''
				});
			}
		},
		onKeyUp: function(event) {
			if (event.key === "Enter") {
				this.sendMessage();
			}
		},
		render: function() {
			return (
				<div className="chatinput">
					<input 	type="text" 
							className="chatinput-input" 
							placeholder="Send message..." 
							value={this.state.inputMessage} 
							onChange={this.inputMessageChange}
							onKeyUp={this.onKeyUp} />
				</div>
			);
		}
	});

	return ChatInput;
});