define([ "react"
], function(React) {

	var ChatMessage = React.createClass({
		render: function() {
			var className;
			if (this.props.author === 'Author2') {
				className = "chatmsg me";
			} else {
				className = "chatmsg other";
			}

			return (
				<div className={className}>
					<span className="chatmsg-author">
						{this.props.author}
					</span>
					<span className="chatmsg-text">
						{this.props.text}
					</span>
				</div>
			);
		}
	});

	return ChatMessage;
});