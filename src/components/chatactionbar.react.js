define([ "react"
], function(React) {

	var ChatMessage = React.createClass({
		render: function() {
			return (
				<div className="actionbar">
					<div className="name">
						Someone
					</div>
					<div className="actions">
						<div className="action opacity-enter">
							<span className="icon icon-settings"></span>
						</div>
						<div className="action opacity-enter">
							<span className="icon icon-close"></span>
						</div>
					</div>
				</div>
			);
		}
	});

	return ChatMessage;
});