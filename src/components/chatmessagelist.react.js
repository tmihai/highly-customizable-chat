define([ "react",
		 "chatmessage"
], function(React, ChatMessage) {

	var ChatMessageList = React.createClass({
		render: function() {
			var messageNodes = this.props.messages.map(function (message) {
			    return (
			        <ChatMessage key={message.timestamp} author={message.author} text={message.text} />
			   	);
			});
			return (
			    <div className="messages">
			    	{messageNodes}
			    </div>
			);
		}
	});

	return ChatMessageList;
});