define([ "react",
		 "chatactionbar",
		 "chatmessagelist",
		 "chatinput",
		 "MessagesStore",
		 "MessagesDispatcher"
], function(React, ChatActionBar, ChatMessageList, ChatInput, MessagesStore, MessagesDispatcher) {
	
	var ChatTab = React.createClass({
		getInitialState: function(){
			return {
				messages: MessagesStore.messages
			};
		},
		sendMessage: function(messageText) {
			MessagesStore.addMessage(messageText)
			this.setState({
				messages: MessagesStore.messages
			});
		},
		render: function() {
			return (
				<div className="chat">
					<ChatActionBar />
					<ChatMessageList messages={this.state.messages} />
					<ChatInput sendMessage={this.sendMessage} />
				</div>
			);
		}
	});

	return ChatTab;
});